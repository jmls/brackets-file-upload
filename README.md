# brackets-file-upload #

nodeSpeed Development Brackets IDE extension to allow file upload to a selected project file tree directory. 

### Installation ###

Extension can be installed from Brackets Extension Manager, or manually placing extension source files into Brackets extension folder.

Once the extension is installed and Brackets is reloaded, file tree context menu is extended with 'Upload files...' option:

### Usage ###

There are two ways to upload files to server: by selecting files/folder from client file system by using UI buttons, or just drag and dropping files and folders on UI drop-zone.  

As files being processed you can see each file status in the log.

If file with the same name already exists on the server, user gets prompted on the fly to whether overwrite the file:

### nodeSpeed Development ###

This extension provides some missing functionality from core Brackets running in a web browser with nodeSpeed Development.
