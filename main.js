/*jslint vars: true, plusplus: true, devel: true, nomen: true, regexp: true, indent: 4, maxerr: 50 */
/*global define, $, brackets, FileReader*/

define(function (require, exports, module) {
    'use strict';

    // Brckets modules
    var CommandManager = brackets.getModule('command/CommandManager'),
        Menus = brackets.getModule('command/Menus'),
        ProjectManager = brackets.getModule('project/ProjectManager'),
        AppInit = brackets.getModule('utils/AppInit'),
        Dialogs = brackets.getModule('widgets/Dialogs'),
        DefaultDialogs = brackets.getModule('widgets/DefaultDialogs'),
        ExtensionUtils = brackets.getModule('utils/ExtensionUtils'),
        Mustache = brackets.getModule("thirdparty/mustache/mustache"),
        dialogTemplate = require('text!templates/uploadTemplate.html'),
        NodeDomain = brackets.getModule('utils/NodeDomain'),
        overwriteAll = false,
        dir = '',
        fileList = [];

    var async = require('node_modules/async/dist/async');

    var domain_path = ExtensionUtils.getModulePath(module, 'node/fileUploadDomain'),
        domain = new NodeDomain('fileUploadDomain', domain_path);

    function FileDataModel(file, path) {
        this.file = file;
        this.path = path;
    }

    ExtensionUtils.loadStyleSheet(module, 'styles/style.less');

    function showOverwriteDialog(name, callback) {
        Dialogs.showModalDialog(
                DefaultDialogs.DIALOG_ID_EXT_DELETED, 'File exists ', 'File ' + name + ' already exists in this directory, do you want to overwrite it?', [{
                    className: Dialogs.DIALOG_BTN_CLASS_NORMAL,
                    id: 'no',
                    text: 'No'
                }, {
                    className: Dialogs.DIALOG_BTN_CLASS_NORMAL,
                    id: 'yes-to-all',
                    text: 'Yes to All'
                }, {
                    className: Dialogs.DIALOG_BTN_CLASS_PRIMARY,
                    id: 'yes',
                    text: 'Yes'
                }]
            )
            .done(function (id) {
                callback(id);
            });
    }

    function handleUploadProcess(files, iterator) {
        async.waterfall([
                function readFileContent(callback) {

                    var reader = new FileReader();
                    reader.onload = function (event) {
                        // get file content
                        callback(null, {
                            name: (files[iterator].path),
                            file: event.target.result
                        });
                    };
                    reader.readAsBinaryString(files[iterator].file);
                },
                function checkIfFileExists(fileData, callback) {

                    domain.exec('checkFile', dir + fileData.name).done(function (data) {
                        fileData.existStatus = data;
                        callback(null, fileData);
                    });
                },
                function overwriteDialog(fileData, callback) {

                    if (!fileData.existStatus || overwriteAll) {
                        fileData.writeStatus = 'yes';
                        callback(null, fileData);
                    } else {
                        showOverwriteDialog(fileData.name, function (data) {
                            fileData.writeStatus = data;

                            if (fileData.writeStatus === 'yes-to-all') {
                                overwriteAll = true;
                            }

                            callback(null, fileData);
                        });
                    }
                },
                function writeFile(fileData, callback) {

                    if (fileData.writeStatus === 'yes' || fileData.writeStatus === 'yes-to-all') {
                        domain.exec('writeFile', dir + fileData.name, fileData.file).done(function () {
                            callback(null, {
                                iterator: iterator,
                                message: dir + fileData.name + ' was uploaded.'
                            });
                        });
                    } else {
                        callback(null, {
                            iterator: iterator,
                            message: dir + fileData.name + ' was skipped.'
                        });
                    }
                }
            ],
            function (err, result) {

                $('#status').text((result.iterator + 1) + '/' + files.length + ' files processed.');
                $('#upload-log').prepend('<p id=\'item\'>' + result.message + '</p>');

                if (result.iterator + 1 === files.length) {
                    $('#upload-log').prepend('<p id=\'item\'>Upload done - ' + (result.iterator + 1) + '/' + files.length + ' files processed.' + '</p>');
                }

                if (iterator === files.length - 1) {
                    ProjectManager.refreshFileTree();
                }
                if (iterator++ < files.length - 1) {
                    handleUploadProcess(files, iterator++);
                }
            });
    }

    // keep track of recursion count
    var count = 0;

    // to be called when recursion is completed
    function last() {
        handleUploadProcess(fileList, 0);
    }

    // called every recursion
    var done = function () {
        if (count === 0) {
            last();
        }
    };

    function traverseFileTree(item, path) {
        count++;
        path = path || '';

        if (item.isFile) {
            // Get file
            item.file(function (file) {

                fileList.push(new FileDataModel(file, path + file.name));

                count--;
                done();
            });
        } else if (item.isDirectory) {
            // Get folder contents
            var dirReader = item.createReader();
            dirReader.readEntries(function (entries) {

                for (var i = 0; i < entries.length; i++) {
                    traverseFileTree(entries[i], path + item.name + '/');
                }
                count--;
                done();
            });
        }
    }

    function showDialog() {
        var context = {
            upload_dir: dir
        };

        Dialogs.showModalDialog(DefaultDialogs.DIALOG_ID_INFO, 'Upload files', Mustache.render(dialogTemplate, context));

        // Handle the file inputs
        var $dropzone = $('#upload-drop-zone');
        var $fileInput = $('#file-input');
        var $folderInput = $('#folder-input');
        var $uploadLog = $('#upload-log');

        $uploadLog.hide();

        $fileInput.on('change', function () {
            var fileData = [];
            overwriteAll = false;

            async.each(this.files, function (file, callback) {
                fileData.push(new FileDataModel(file, file.name));
                callback();
            }, function () {
                $uploadLog.show();
                handleUploadProcess(fileData, 0);
                $fileInput.val('');
            });
        });

        $folderInput.on('change', function () {
            var fileData = [];
            overwriteAll = false;

            async.each(this.files, function (file, callback) {
                fileData.push(new FileDataModel(file, file.webkitRelativePath));
                callback();
            }, function () {
                $uploadLog.show();
                handleUploadProcess(fileData, 0);
                $folderInput.val('');
            });
        });

        $dropzone.on('dragenter', function (e) {
            e.stopPropagation();
            e.preventDefault();
            $dropzone.css('background-color', 'rgba(210, 210, 210, 0.8)');
        });

        $dropzone.on('dragleave', function (e) {
            e.stopPropagation();
            e.preventDefault();
            $dropzone.css('background-color', 'rgba(210, 210, 210, 0.35)');
        });

        $dropzone.on('dragover', function (e) {
            e.stopPropagation();
            e.preventDefault();
        });

        $dropzone.on('drop', function (e) {
            e.preventDefault();
            e.stopPropagation();
            $dropzone.css('background-color', 'rgba(210, 210, 210, 0.35)');

            overwriteAll = false;
            fileList = [];
            $uploadLog.show();

            var items = e.originalEvent.dataTransfer.items;

            for (var i = 0; i < items.length; i++) {
                var item = items[i].webkitGetAsEntry();

                if (item) {
                    traverseFileTree(item);
                }
            }
        });
    }

    AppInit.htmlReady(function () {

        var context_menus = Menus.getContextMenu(Menus.ContextMenuIds.PROJECT_MENU);
        var UPLOAD_FILE = 'fileUpload.upload';

        CommandManager.register('Upload files...', UPLOAD_FILE, showDialog);
        context_menus.addMenuDivider();
        context_menus.addMenuItem(UPLOAD_FILE, null, Menus.LAST);

        context_menus.on('beforeContextMenuOpen', function () {

            var selectedEntry = ProjectManager.getSelectedItem();
            dir = (selectedEntry._isDirectory) ? selectedEntry._path : selectedEntry._parentPath;
        });
    });
});