/*jslint vars: true, plusplus: true, devel: true, nomen: true, regexp: true, indent: 4, maxerr: 50, white: true */
/*global require, exports*/

(function () {

    "use strict";

    var _domainManager;

    // Node Modules
    var fs = require('fs');
    var mkdirp = require('mkdirp');

    /**
     * @private
     * Handler function for writting file to disk.
     * @param {string} Path to file.
     * @param {object} File binnary string.
     */
    function writeFile(path, file) {
        mkdirp(path.substr(0, path.lastIndexOf('/')), function (err) {

            if (err) {
                return (err);
            }

            fs.writeFile(path, file, 'binary');
        });
    }

    /**
     * @private
     * Handler function for checking if file exists on disk.
     * @param {string} Path to file.
     * @return {string} result.
     */
    function fileExists(filePath) {
        try {
            return fs.statSync(filePath).isFile();
        } catch (err) {
            return false;
        }
    }

    // Init
    function init(domainManager) {

        _domainManager = domainManager;

        if (!domainManager.hasDomain('fileUploadDomain')) {

            domainManager.registerDomain('fileUploadDomain', {
                major: 0,
                minor: 1
            });
        }

        _domainManager.registerCommand('fileUploadDomain', 'checkFile', fileExists, false, 'Checks if file exists on server', [{
            name: 'path',
            type: 'string',
            decription: 'Full path of a file to be written'
        }], [{
            name: 'data', // return value
            type: 'object',
            description: 'Upload status'
        }]);

        _domainManager.registerCommand('fileUploadDomain', 'writeFile', writeFile, false, 'Writes file to server', [{
            name: 'path',
            type: 'string',
            decription: 'Full path of a file to be written'
        }, {
            name: 'file',
            type: 'object',
            decription: 'File to be written to disk'
        }], [{
            name: 'data',
            type: 'object',
            decription: 'File write process response'
        }]);
    }

    exports.init = init;
}());